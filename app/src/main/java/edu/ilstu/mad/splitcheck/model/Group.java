package edu.ilstu.mad.splitcheck.model;

import java.util.ArrayList;

/**
 * This represents a payment group that holds group members information.
 */
public class Group {
    private String groupId;
    private ArrayList<Person> arrayOfPeople;

    public Group()
    {
        arrayOfPeople = new ArrayList<Person>();
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public ArrayList<Person> getArrayOfPeople() {
        return arrayOfPeople;
    }

    public void setArrayOfPeople(ArrayList<Person> arrayOfPeople) {
        this.arrayOfPeople = arrayOfPeople;
    }
    public void addPerson(String name)
    {
        Person newPerson = new Person(name);
        arrayOfPeople.add(newPerson);

    }
}
