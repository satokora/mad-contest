package edu.ilstu.mad.splitcheck.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class represents a Person object that keeps track of splitted money amount
 */
public class Person implements Serializable{
    private String name;
    private String url;
    private Integer imgId; // profile image id
    private double totalAmount;
    private ArrayList<Item> arrayOfItem;


    public Person(){
        super();
    }
    public Person(String name)
    {
        this.name = name;
        this.url = "https://www.paypal.me/satokokora";
    }

    public Person(String name, double totalAmount){
        this.name=name;
        this.totalAmount=totalAmount;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getImgId() {
        return imgId;
    }

    public void setImgId(Integer imgId) {
        this.imgId = imgId;
    }

    public double addPriceToTotalAmount(double price)
    {
        totalAmount += price;
        return totalAmount;
    }
}
