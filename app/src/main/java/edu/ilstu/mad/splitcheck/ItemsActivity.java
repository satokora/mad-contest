package edu.ilstu.mad.splitcheck;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import edu.ilstu.mad.splitcheck.model.Item;
import edu.ilstu.mad.splitcheck.model.Person;

public class ItemsActivity extends AppCompatActivity {

    final String INTENT_EXTRA_ARRAY_PEOPLE="People";
    private static ArrayList<Person> peopleInGroup;
    private ArrayList<Item> itemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);


        Intent intent = getIntent();
        peopleInGroup = new ArrayList<Person>();
        peopleInGroup = (ArrayList<Person>)intent.getSerializableExtra(INTENT_EXTRA_ARRAY_PEOPLE);

        for (int i=0;i<peopleInGroup.size();i++)
        {
            Log.i("SplitCheck", peopleInGroup.get(i).getName() + peopleInGroup.get(i).getUrl());
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        setContentView(R.layout.activity_items);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


        ListView listView;
        ListAdapter adapter;

        listView=(ListView)findViewById(R.id.listView3);
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                DialogFragment newFragment = new SplitDialogFragment();
//                newFragment.show(getFragmentManager(), "test");
//                Bundle bundle = new Bundle();
//                bundle.putString("edttext", "From Activity");
//                Fragmentclass fragobj = new Fragmentclass();
//                fragobj.setArguments(bundle);
//            }
//        });

         itemList = new ArrayList<Item>();
        Item temp= new Item("Caffe Latte Mocha",3.82);
        itemList.add(temp);
        temp= new Item("Frontega Chicken",7.69);
        itemList.add(temp);
        temp= new Item("Steak & White Cheddar",8.79);
        itemList.add(temp);
        temp= new Item("BBQ Chicken Sala",8.49);
        itemList.add(temp);
        String[] i1={"Caffe Latte Mocha","Frontega Chicken","Steak & White Cheddar","BBQ Chicken Salad"};
        Double[] i2={3.82,7.69,8.79,8.49};

        //personCardAdapter = new CompleteListAdapter(this, names,imgid);
        adapter= new CustomListAdapter(this,i1,i2);
        listView.setAdapter(adapter);

        Button doneButton = (Button)findViewById(R.id.splitdone);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ItemsActivity.this, BillSplit.class);
                intent.putExtra("myList", peopleInGroup);
                startActivity(intent);
                finish();
            }
        });
    }


        private class CustomListAdapter extends ArrayAdapter<String> {

        //private final Activity context;
            private Activity context;
            private String[] itemName;
            private Double[] itemPrice;

        public CustomListAdapter(Activity context, String[] itemName, Double[] itemPrice) {
            super(context, R.layout.message, itemName);


            this.context=context;
            this.itemName=itemName;
            this.itemPrice=itemPrice;
        }



        public View getView(int position,View view,ViewGroup parent) {

            final int selectedPos = position;

            Log.i("I ", "In Here");
            LayoutInflater inflater=context.getLayoutInflater();
            View rowView=inflater.inflate(R.layout.childlayout, null, true);

            TextView txtTitle = (TextView) rowView.findViewById(R.id.textView);
            TextView txtTitle2 = (TextView) rowView.findViewById(R.id.textView2);

            Button button= (Button)rowView.findViewById(R.id.button);
            button.setTag(position);
            button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    DialogFragment newFragment = new SplitDialogFragment();
                    newFragment.show(getFragmentManager(), "test");
                    Bundle bundle = new Bundle();
                    bundle.putString("itemname", itemList.get((int)v.getTag()).getName());
                    bundle.putString("itemprice", itemList.get((int)v.getTag()).getPrice() + "");
                    newFragment.setArguments(bundle);
                }
            });

            Button button2= (Button)rowView.findViewById(R.id.button2);
            button2.setTag(position);
            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DialogFragment newFragment = new PaidByDialogFragment();
                    newFragment.show(getFragmentManager(), "test");
                    Bundle bundle = new Bundle();
                    bundle.putString("itemname", itemList.get((int)v.getTag()).getName());
                    bundle.putString("itemprice", itemList.get((int)v.getTag()).getPrice() + "");
                    newFragment.setArguments(bundle);
                }
            });

            String x= itemPrice[position]+"";
            txtTitle.setText(itemName[position]);
            txtTitle2.setText(x);
            return rowView;

        };
    }

    public static class SplitDialogFragment extends DialogFragment {
        private static ArrayList<Person> selectedPeople;
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            selectedPeople = new ArrayList<Person>();
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View content = inflater.inflate(R.layout.dialog_split, null);

            String strItemName = getArguments().getString("itemname");
            final double strPrice = Double.parseDouble(getArguments().getString("itemprice"));
            builder.setView(content);

            final ListView listView;
            ListAdapter adapter;

            listView=(ListView)content.findViewById(R.id.listPeopleInGroup);

            String personNames[]=new String[peopleInGroup.size()];
            for (int i=0;i<personNames.length;i++)
            {
                personNames[i]=peopleInGroup.get(i).getName();
            }


            //personCardAdapter = new CompleteListAdapter(this, names,imgid);
            adapter= new SplitListAdapter(getActivity(),personNames);
            listView.setAdapter(adapter);

            builder.setMessage(strItemName + " " + strPrice)
                    .setNegativeButton("Done", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            addPriceToTotal(selectedPeople,strPrice);

                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
        private class SplitListAdapter extends ArrayAdapter<String> {

            //private final Activity context;
            private Activity context;
            private String[] personName;
            //private Double[] itemPrice;

            public SplitListAdapter(Activity context, String[] personName) {
                super(context, R.layout.message, personName);


                this.context=context;
                this.personName=personName;
                //this.itemPrice=itemPrice;
            }



            public View getView(int position,View view,ViewGroup parent) {



                LayoutInflater inflater=context.getLayoutInflater();
                View rowView=inflater.inflate(R.layout.select_person_split, null, true);

                TextView txtTitle = (TextView) rowView.findViewById(R.id.personname);
                ImageView imageView = (ImageView) rowView.findViewById(R.id.personPay);
                CheckBox checkBox = (CheckBox) rowView.findViewById(R.id.checkBox);
                checkBox.setTag(position);
                for (int i=0;i<peopleInGroup.size();i++)
                {
                    if (peopleInGroup.get(i).getName().equalsIgnoreCase(personName[position]))
                    {
                        imageView.setImageResource(peopleInGroup.get(i).getImgId());
                    }
                }

                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            int pos = (int)buttonView.getTag();
                            selectedPeople.add(peopleInGroup.get(pos));
                        }
                        else
                        {
                            int pos = (int)buttonView.getTag();
                            for (int i=0;i<selectedPeople.size();i++)
                            {
                                if (selectedPeople.get(i).getName().equalsIgnoreCase(peopleInGroup.get(pos).getName()))
                                {
                                    selectedPeople.remove(i);
                                }
                            }

                        }

                    }
                });

                txtTitle.setText(personName[position]);



                return rowView;

            };
        }
    }
    public static class PaidByDialogFragment extends DialogFragment {
        private static Person selectedPerson;
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View content = inflater.inflate(R.layout.dialog_split, null);

            String strItemName = getArguments().getString("itemname");
            final double strPrice = Double.parseDouble(getArguments().getString("itemprice"));
            builder.setView(content);

            ListView listView;
            ListAdapter adapter;

            listView=(ListView)content.findViewById(R.id.listPeopleInGroup);

            String personNames[]=new String[peopleInGroup.size()];
            for (int i=0;i<personNames.length;i++)
            {
                personNames[i]=peopleInGroup.get(i).getName();
            }


            //personCardAdapter = new CompleteListAdapter(this, names,imgid);
            adapter= new SplitListAdapter(getActivity(),personNames);
            listView.setAdapter(adapter);

            builder.setMessage(strItemName + " " + strPrice)
                    .setNegativeButton("Done", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            addPriceToOnePerson(selectedPerson,strPrice);
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
        private class SplitListAdapter extends ArrayAdapter<String> {

            //private final Activity context;
            private Activity context;
            private String[] personName;
            //private Double[] itemPrice;

            public SplitListAdapter(Activity context, String[] personName) {
                super(context, R.layout.message, personName);


                this.context=context;
                this.personName=personName;
                //this.itemPrice=itemPrice;
            }



            public View getView(int position,View view,ViewGroup parent) {



                LayoutInflater inflater=context.getLayoutInflater();
                View rowView=inflater.inflate(R.layout.select_person_paid_by, null, true);

                TextView txtTitle = (TextView) rowView.findViewById(R.id.personname);
                ImageView imageView = (ImageView) rowView.findViewById(R.id.personPay);
                RadioButton radio = (RadioButton) rowView.findViewById(R.id.radioButton);
                radio.setTag(position);
                for (int i=0;i<peopleInGroup.size();i++)
                {
                    if (peopleInGroup.get(i).getName().equalsIgnoreCase(personName[position]))
                    {
                        imageView.setImageResource(peopleInGroup.get(i).getImgId());
                    }
                }

                radio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            int pos = (int)buttonView.getTag();
                            selectedPerson=peopleInGroup.get(pos);
                        }


                    }
                });

                txtTitle.setText(personName[position]);

                return rowView;

            };
        }
    }
    public static void addPriceToTotal(ArrayList<Person> selectedPpl, double price)
    {
        double evenPrice = price / selectedPpl.size();
        for (int i = 0; i<selectedPpl.size(); i++)
        {
            selectedPpl.get(i).addPriceToTotalAmount(evenPrice);
            Log.i("SplitCheck",selectedPpl.get(i).getName() + "will pay $" + selectedPpl.get(i).getTotalAmount());
        }
    }
    public static void addPriceToOnePerson(Person selectedPerson, double price)
    {

        selectedPerson.addPriceToTotalAmount(price);
        Log.i("SplitCheck",selectedPerson.getName() + "will pay $" + selectedPerson.getTotalAmount());

    }
}
