package edu.ilstu.mad.splitcheck;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.ilstu.mad.splitcheck.model.Person;

public class BillSplit extends AppCompatActivity {

    private ArrayList<Person> paymentGroup;
    ListView personListView;
    private List<String> mItems;
    private CustomListAdapter personCardAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_split);

        // System.out.print("In here!");

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


        Log.i("BillSplit", "beforeSetAdapter");

//        ListView listView = (ListView) findViewById(R.id.listView2);
//        ArrayAdapter<String> itemsAdapter =
//                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
//        listView.setAdapter(itemsAdapter);

        ListView listView;
        ListAdapter adapter;

        listView=(ListView)findViewById(R.id.listView2);
        //Bundle wrapedReceivedList = getIntent().getExtras();
        //ArrayList<Person> p = new ArrayList<Person>();



        //Main Code
        Intent intent = getIntent();
        paymentGroup = new ArrayList<Person>();
        paymentGroup = (ArrayList<Person>) intent.getSerializableExtra("myList");



        String[] pplArray = new String[paymentGroup.size()];
        int c=-1;
        Iterator<Person> i= paymentGroup.iterator();
        while (i.hasNext()){
            c++;
            Person temp=i.next();
            pplArray[c]=temp.getName()+" owes you  $"+temp.getTotalAmount();
            Log.i("BillSplit", pplArray[c]);
            //itemsAdapter.add(pplArray[c]);
        }

        adapter= new CustomListAdapter(this,pplArray);
        listView.setAdapter(adapter);

        //personCardAdapter = new CompleteListAdapter(this, names,imgid);

        //personListView.setAdapter(personCardAdapter);
//        personListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//
//
//            }
//        });



//        //Testing Code
//        ArrayList<Person> p1= new ArrayList<Person>();
//        Person t= new Person("Sopan", 20);
//        p1.add(t);
//        t=new Person("Satoko", 30);
//        p1.add(t);
//        t=new Person("Frank", 40);
//        p1.add(t);
//
//        String[] pplArray = new String[10];
//        int c=-1;
//        Iterator<Person> i= p1.iterator();
//        while (i.hasNext()){
//            c++;
//            Person temp=i.next();
//            pplArray[c]=temp.getName()+"   "+temp.getTotalAmount();
//            itemsAdapter.add(pplArray[c]);
//        }



        //ListView listView = (ListView) findViewById(R.id.listView2);

    }


    private class CustomListAdapter extends ArrayAdapter<String> {

        //private final Activity context;
        private Activity context;
        private String[] itemName;
        //private Double[] itemPrice;

        public CustomListAdapter(Activity context, String[] itemName) {
            super(context, R.layout.message, itemName);


            this.context=context;
            this.itemName=itemName;
            //this.itemPrice=itemPrice;
        }



        public View getView(int position,View view,ViewGroup parent) {

            final int selectedPos = position;

            Log.i("I ", "In Here");
            LayoutInflater inflater=context.getLayoutInflater();
            View rowView=inflater.inflate(R.layout.resultitem, null, true);

            TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
            TextView txtTitle2 = (TextView) rowView.findViewById(R.id.cur);

            ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView);
            for (int i = 0;i<paymentGroup.size();i++)
            {
                if (itemName[position].contains(paymentGroup.get(i).getName()))
                {
                    imageView.setImageResource(paymentGroup.get(i).getImgId());
                }
            }

            Button button= (Button)rowView.findViewById(R.id.send);
            button.setTag(position);
            button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                    DialogFragment newFragment = new SplitDialogFragment();
//                    newFragment.show(getFragmentManager(), "test");
//                    Bundle bundle = new Bundle();
//                    bundle.putString("itemname", itemList.get((int)v.getTag()).getName());
//                    bundle.putString("itemprice", itemList.get((int)v.getTag()).getPrice() + "");
//                    newFragment.setArguments(bundle);
                }
            });



            //String x= itemPrice[position]+"";
            txtTitle.setText(itemName[position]);
            //txtTitle2.setText(x);
            return rowView;

        };
    }

}
