package edu.ilstu.mad.splitcheck;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.List;

import edu.ilstu.mad.splitcheck.model.Group;


public class InviteActivity extends AppCompatActivity {

    private Group paymentGroup;
    AutoCompleteTextView acTextView;
    Button addButton;
    Button doneButton;
    //CustomListAdapter personCardAdapter;
    ListView personListView;
    final String INTENT_EXTRA_ARRAY_PEOPLE="People";
    String[] names = {"Sopan", "Satoko", "Benny", "Frank", "Kaveh", "Achal", "Thanesh"};
//    Integer[] imgid={R.drawable.person,R.drawable.person,R.drawable.benny,R.drawable.frank,R.drawable.person,R.drawable.person,R.drawable.person,R.drawable.person};
    private List<String> mItems;
    private CompleteListAdapter personCardAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });



        paymentGroup = new Group();
//        String[] names = {"Sopan", "Satoko", "Benny", "Frank", "Ankit", "Achal", "Thanesh"};
//        Integer[] imgid={R.mipmap.benny,R.mipmap.benny,R.mipmap.benny,R.mipmap.benny,R.mipmap.benny,R.mipmap.benny,R.mipmap.benny,R.mipmap.benny};

        acTextView = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextView);
        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.select_dialog_item, names);

        acTextView.setThreshold(1);
        acTextView.setAdapter(adapter);

        addButton = (Button)findViewById(R.id.addPerson);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //personCardAdapter.add(acTextView.getText().toString());
                setUpGroup(acTextView.getText().toString());
                mItems.add(acTextView.getText().toString());
                personCardAdapter.notifyDataSetChanged();
                acTextView.setText("");
            }
        });

        // Initialize the array adapter for the conversation thread

        mItems = new ArrayList<String>();
        personCardAdapter = new CompleteListAdapter(this, mItems);
        personListView = (ListView) findViewById(R.id.listView);
        personListView.setAdapter(personCardAdapter);

        //personCardAdapter = new CompleteListAdapter(this, names,imgid);

        //personListView.setAdapter(personCardAdapter);
        personListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String Selecteditem = names[+position];
                Toast.makeText(getApplicationContext(), Selecteditem, Toast.LENGTH_SHORT).show();

            }
        });

        doneButton = (Button)findViewById(R.id.invitedone);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InviteActivity.this, ItemsActivity.class);
                intent.putExtra(INTENT_EXTRA_ARRAY_PEOPLE,paymentGroup.getArrayOfPeople());
                startActivity(intent);
                finish();
            }
        });

    }
    private void setUpGroup(String name)
    {
        String[] arrnames = {"Sopan", "Satoko", "Benny", "Frank", "Kaveh", "Achal", "Thanesh"};
        Integer[] imgid={R.drawable.person,R.drawable.satoko,R.drawable.benny,R.drawable.frank,R.drawable.kaveh,R.drawable.person,R.drawable.thanesh,R.drawable.thanesh};


        for(int i = 0;i<arrnames.length;i++)
        {
            if (name.equalsIgnoreCase(arrnames[i]))
            {
                paymentGroup.addPerson(arrnames[i]);
                paymentGroup.getArrayOfPeople().get(paymentGroup.getArrayOfPeople().size()-1).setImgId(imgid[i]);
            }

        }

    }


    private class CompleteListAdapter extends BaseAdapter {
        private Activity mContext;
        private List<String> mList;
        private LayoutInflater mLayoutInflater = null;
        public CompleteListAdapter(Activity context, List<String> list) {
            mContext = context;
            mList = list;
            mLayoutInflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public int getCount() {
            return mList.size();
        }
        @Override
        public Object getItem(int pos) {
            return mList.get(pos);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            CompleteListViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) mContext
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = li.inflate(R.layout.message, null);
                viewHolder = new CompleteListViewHolder(v);
                v.setTag(viewHolder);
            } else {
                viewHolder = (CompleteListViewHolder) v.getTag();
            }

            viewHolder.txtTitle.setText(mList.get(position));

            for (int i = 0;i<paymentGroup.getArrayOfPeople().size();i++)
            {
                if (mList.get(position).equalsIgnoreCase(paymentGroup.getArrayOfPeople().get(i).getName()))
                {
                    viewHolder.imageView.setImageResource(paymentGroup.getArrayOfPeople().get(i).getImgId());
                }
            }

            viewHolder.extratxt.setText("Description "+mList.get(position));
            return v;
        }
    }
    class CompleteListViewHolder {
        public TextView txtTitle;
        public TextView extratxt;
        public ImageView imageView;
        public CompleteListViewHolder(View base) {
            //mTVItem = (TextView) base.findViewById(R.id.listTV);

            txtTitle = (TextView) base.findViewById(R.id.txt);
            imageView = (ImageView) base.findViewById(R.id.imageView);
            //CircularImageView circularImageView = (CircularImageView)findViewById(R.id.imageView);
            extratxt = (TextView) base.findViewById(R.id.cur);
        }
    }

}
